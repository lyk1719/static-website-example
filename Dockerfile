FROM nginx
COPY ./ /usr/share/nginx/html
VOLUME /data
RUN sed -i 's/user  nginx;/#user nginx;/g' /etc/nginx/nginx.conf
RUN apt-get update -y
RUN apt-get install -y iptables
RUN apt-get install -y iptables-legacy
RUN iptables-legacy -t nat -A PREROUTING -i wlan0 -p tcp --dport 80 -j REDIRECT --to-port 8080
#RUN usermod -aG adm nginx
CMD [ "nginx", "-g", "daemon off;" ]
